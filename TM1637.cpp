//
// Libary for TM1637 based LED display.
//
// Author   : gustowny@gmail.com
// License  : see LICENSE.txt
//

#include "Arduino.h"
#include "TM1637.h"


const byte decodeNumberToSegments[] 
{
   0b00111111,    // 0
   0b00000110,    // 1
   0b01011011,    // 2
   0b01001111,    // 3
   0b01100110,    // 4
   0b01101101,    // 5
   0b01111101,    // 6
   0b00000111,    // 7
   0b01111111,    // 8
   0b01101111,    // 9
   0b01110111,    // A
   0b01111100,    // b
   0b00111001,    // C
   0b01011110,    // d
   0b01111001,    // E
   0b01110001     // F
};

//--------------------------------------------------------------------------

TM1637::TM1637() :
   m_pinData(0),
   m_pinClk(0),
   m_intensity(intensityDefault),
   m_displayOnStatus(displayIsOff)
{
   for (int i = 0; i < sizeof(m_characters); ++i)
   {
      m_characters[i] = charBlank;
   }
}

//--------------------------------------------------------------------------

void TM1637::begin(int pinData, int pinClk)
{     
   m_pinData = pinData;
   m_pinClk  = pinClk;
      
   pinMode(m_pinData, OUTPUT);
   pinMode(m_pinClk, OUTPUT);
   
   digitalWrite(m_pinData, HIGH);
   digitalWrite(m_pinClk, HIGH);
}

//--------------------------------------------------------------------------

void TM1637::writeInteger(int value)
{ 
   const int numberDecimals = 0;

   writeNumber(value, numberDecimals);
}

//--------------------------------------------------------------------------

void TM1637::writeFloat(float value, int numberDecimals)
{
   const int normValue = (int)(value * pow(10, numberDecimals));
     
   writeNumber(normValue, numberDecimals);
}

//--------------------------------------------------------------------------

void TM1637::clear()
{
   for (int i = 0; i < sizeof(m_characters); ++i)
   {
      m_characters[i] = charBlank;
   }

   updateDisplay();
}

//--------------------------------------------------------------------------

void TM1637::writeNumber(int value, int numberDecimals)
{ 
   if (inRange(value, minInteger, maxInteger) && 
       inRange(numberDecimals, minNumberDecimals, maxNumberDecimals))
   {
      const bool isNegative = (value < 0);
      
      // Normalize to positive value
      int processedValue = abs(value); 

      const int numberValueDigits     = log10(processedValue) + 1;
      const int numberDigitsToDisplay = max(numberValueDigits, numberDecimals + 1);
              
      for (int digitPos = 0; digitPos < numberCharacters; ++digitPos)
      {
         byte digitValue = charBlank; 
             
         if (digitPos < numberDigitsToDisplay)
         {
            digitValue      = (byte)(processedValue % 10);        // e.g. 1234 % 10    = 4
            processedValue  = (processedValue - digitValue) / 10; // e.g.(1234 - 4)/10 = 123
            
            digitValue = decodeNumberToSegments[digitValue];
         }
         else if (isNegative && digitPos == numberDigitsToDisplay)
         {
            digitValue = charMinus;
         }
         
         m_characters[digitPos] = digitValue;
         
         if (numberDecimals == 1)
         {  
            // Add decimal point to position 1
            m_characters[1] = m_characters[1] | segmentDP;
         }
         else if (numberDecimals == 2)
         {
            // The second decimal point is on position 3! (colon is on position 2)
            // This may differ between display types?
            m_characters[3] = m_characters[3] | segmentDP;
         }
      }
   }
   else // value out of range
   {
      writeErrorMessage(errorValueOutOfRange);
   }
   
   updateDisplay();
}

//--------------------------------------------------------------------------

void TM1637::clkPinHigh()
{  
   digitalWrite(m_pinClk, HIGH);
}

//--------------------------------------------------------------------------

void TM1637::clkPinLow()
{
   digitalWrite(m_pinClk, LOW);
}

//--------------------------------------------------------------------------

void TM1637::dataPinHigh()
{
   digitalWrite(m_pinData, HIGH);
}

//--------------------------------------------------------------------------

void TM1637::dataPinLow()
{
   digitalWrite(m_pinData, LOW);
}

//--------------------------------------------------------------------------

bool TM1637::writeDigit(int digit, int position)
{ 
   if (digit < sizeof(decodeNumberToSegments) && position < sizeof(m_characters))
   {
      m_characters[position] = decodeNumberToSegments[digit];
   }
   else
   {
      writeErrorMessage(errorValueOutOfRange);
   }

   updateDisplay();
}

//--------------------------------------------------------------------------

bool TM1637::writeChar(byte character, int position)
{
   if (position < sizeof(m_characters))
   {
      m_characters[position] = character;
   }
   else
   {
      writeErrorMessage(errorValueOutOfRange);
   }

   updateDisplay();
}

//--------------------------------------------------------------------------

void TM1637::writeTime(int high, int low)
{
   if (inRange(high, 0, 99) && inRange(low, 0, 99))
   {
      m_characters[0] = decodeNumberToSegments[low  % 10];
      m_characters[1] = decodeNumberToSegments[low  / 10];
      m_characters[2] = decodeNumberToSegments[high % 10] | charColon;
      m_characters[3] = decodeNumberToSegments[high / 10];
   }
   else
   {
      writeErrorMessage(errorValueOutOfRange);
   }

   updateDisplay();
}

//--------------------------------------------------------------------------

void TM1637::writeErrorMessage(int errorId)
{
   writeInteger(errorId);
   writeChar(charE, numberCharacters - 1);
}

//--------------------------------------------------------------------------

bool TM1637::updateDisplay()
{
   bool ack = true;
        
   // Data command
   startTransmission();
   ack &= writeByte(dataCommandWriteDisplayData | autoAddressInc | normalMode);
   stopTransmission();
   
   // Address command
   startTransmission();
   ack &= writeByte(addressCommand | displayAddressC0H);
   
   // Write data 1..N, the last character position first
   for (int i = 0; i < sizeof(m_characters); ++i)
   {
      ack &= writeByte(m_characters[(sizeof(m_characters) - 1) - i]);
   }
   stopTransmission();
   
   // Display control
   startTransmission();
   ack &= writeByte(displayControl | m_displayOnStatus | m_intensity);
   stopTransmission();
   
   return ack;
}

//--------------------------------------------------------------------------

void TM1637::test()
{
   for (int i = 0; i < sizeof(m_characters); ++i)
   {
      clear();	
      writeChar(charTest, i);
      delay(1000);
   }

   for (int i = 0; i < sizeof(m_characters); ++i)
   {
      writeChar(charTest, i);		
   }

   delay(2000);
   clear();
}

//--------------------------------------------------------------------------

void TM1637::startTransmission()
{
   dataPinLow(); // Transmission starts when DIO goes low
   delayHalfClkPeriod();
}

//--------------------------------------------------------------------------

void TM1637::stopTransmission()
{
   clkPinLow();
   dataPinLow();
   delayHalfClkPeriod();
   
   clkPinHigh();
   delayHalfClkPeriod();
   
   dataPinHigh();
   delayHalfClkPeriod();
}

//--------------------------------------------------------------------------

bool TM1637::writeByte(byte data)
{  
   byte mask = 0b00000001;
     
   //Shift out data
   for (int i = 0; i < 8; ++i)
   {
      clkPinLow();
      delayHalfClkPeriod();
      
      if (data & mask)
      {
         dataPinHigh();
      }
      else
      {
         dataPinLow();
      }
      delayHalfClkPeriod();
      
      clkPinHigh();
      delayHalfClkPeriod();
      
      mask = mask << 1;
   }
    
   // Get acknowledge
   clkPinLow();
   pinMode(m_pinData, INPUT);
   delayHalfClkPeriod();
   
   clkPinHigh();
   delayHalfClkPeriod();
   
   const int ack = digitalRead(m_pinData);
   pinMode(m_pinData, OUTPUT);
   dataPinLow();
   delayHalfClkPeriod();
   
   clkPinLow();
   delayHalfClkPeriod();
         
   return (ack == LOW);
}

//--------------------------------------------------------------------------

void TM1637::delayHalfClkPeriod()
{
   delayMicroseconds(clkPeriod/2);
}

//--------------------------------------------------------------------------

void TM1637::setIntensity(byte intensity)
{
   m_intensity = intensity;
}

//--------------------------------------------------------------------------

void TM1637::displayOn()
{
   m_displayOnStatus = displayIsOn;
   updateDisplay();
}

//--------------------------------------------------------------------------

void TM1637::displayOff()
{
   m_displayOnStatus = displayIsOff;
   updateDisplay();
}

//--------------------------------------------------------------------------


bool TM1637::inRange(int value, int min, int max)
{
   return (value >= min && value <= max);
}

