//
// Libary for TM1637 based 4-digit 7-segments LED display.
//
// Author   : gustowny@gmail.com
// License  : see LICENSE.txt
//

#ifndef TM1637_H
#define TM1637_H

#include "Arduino.h"

class TM1637
{
   public:
   
      // Number of characters on the 7-segment display.
      static const int numberCharacters = 4;
      
      // Constructor. Call begin() before using any of the other methods!		
      TM1637();
      
      // Initalizes the display. Default values are set (see below).
      // The display will be off when this method returns.
      void begin(int pinData, int pinClk);
      
      // Writes a single digit (0-9) to given position.
      // The right-most character is position 0.
      bool writeDigit(int digit, int position);
      
      // Writes one of the following characters/segments to given digit position.
      // The right-most character is position 0.
      bool writeChar(byte character, int position);
      
      // Segment bits 
      static const byte segmentA  = 0b00000001;
      static const byte segmentB  = 0b00000010;
      static const byte segmentC  = 0b00000100;
      static const byte segmentD  = 0b00001000;
      static const byte segmentE  = 0b00010000;
      static const byte segmentF  = 0b00100000;
      static const byte segmentG  = 0b01000000;
      static const byte segmentDP = 0b10000000;
      
      /* Segement layout
            
            ---A---
            |     |
            F     B
            |     |
            ---G---
            |     |
            E     C
            |     |
            ---D---(DP)
      */
      
      // Encoded characters           .GFEDCBA
      static const byte charBlank = 0b00000000;
      static const byte charMinus = segmentG;
      static const byte charDot   = segmentDP;	// Only for DECIMAL displays
      static const byte charColon = segmentDP;	// Only for position 2 for COLON displays
      static const byte charTest  = 0b11111111;
      static const byte charZero  = 0b00111111;
      static const byte charE     = 0b01111001;
      
      // Displays a floating point number with fixed number decimals
      void writeFloat(float value, int numberDecimals);
      
      static const int minNumberDecimals = 0;
      static const int maxNumberDecimals = 2;
      
      // Displays an integer on the display.
      void writeInteger(int value);
      
      static const int maxInteger = 9999;
      static const int minInteger = -999;
      
      // Displays two numbers formatted as "high:low".
      void writeTime(int high, int low);
      
      // Executes a display test where all segments of all positions
      // are displayed; first one at a time, and then all together.
      void test();
            
      // Writes an error message on the format "E <errorId>".
      // The 'E' will be in digit position 3.
      // Error IDs > 900 are reserved for internal errors.
      void writeErrorMessage(int errorId);
      
      static const int errorValueOutOfRange = 901;
      
      // Clears the display (no segments are displayed).
      void clear();
      
      // Turns the display on/off.
      // The display can be programmed when it's off, but nothing is displayed.
      void displayOn();
      void displayOff();
      
      // Sets the intensity (brightness) of the LEDs.
      void setIntensity(byte intensity);
      
      static const byte intensityMin     = 0x0;
      static const byte intensityMax     = 0x7;
      static const byte intensityDefault = 0x4;  
         
   private:
   
      int  m_pinData;
      int  m_pinClk;
      int  m_intensity;
      byte m_displayOnStatus;
      
      byte m_characters[4];
  
      void writeNumber(int value, int numberDecimals);
  
      void startTransmission();
      void stopTransmission();
      
      bool writeByte(byte data);
      bool updateDisplay();

      inline void clkPinHigh();
      inline void clkPinLow();
      inline void dataPinHigh();
      inline void dataPinLow();
      inline void delayHalfClkPeriod();
      
      // IO clock period in microseconds.
      const int clkPeriod = 50; 
  
      // Data commands
      const byte dataCommandWriteDisplayData = 0b01000000;
      const byte dataCommandReadKeyData      = 0b01000010;
      
      // Data command modifiers
      const byte autoAddressInc = 0b00000000;
      const byte fixedAddress   = 0b00000100;
      const byte normalMode     = 0b00000000;
      const byte testMode       = 0b00001000;
      
      // Address command
      const byte addressCommand = 0b11000000;
      
      // Address command modifier
      const byte displayAddressC0H = 0x0;
      const byte displayAddressC1H = 0x1;
      const byte displayAddressC2H = 0x2;
      const byte displayAddressC3H = 0x3;
      const byte displayAddressC4H = 0x4;
      const byte displayAddressC5H = 0x5;
      
      // Display control commands
      const byte displayControl = 0b10000000;
      
      // Display control modifier
      const byte pulseWidthMin = 0b00000000;
      const byte pulseWidthMax = 0b00000111;
      const byte displayIsOff  = 0b00000000;
      const byte displayIsOn   = 0b00001000;
      
      bool inRange(int value, int max, int min);
   
      
};

#endif // TM1637_H